#!/bin/bash

mkdir -p debian/squarev/var/lib/squarev

chmod 755 -R debian/squarev/DEBIAN
chmod 644 debian/squarev/DEBIAN/control

./gradlew clean build

G=$(grep "^version =" build.gradle | sed -e "s/version = //g" | sed -e "s/'//g")

if [ -f "build/libs/SQuaReV-$G.jar" ]
then
  echo "created file build/libs/SQuaReV-$G.jar"
else
  >&2 echo "could not find file build/libs/SQuaReV-$G.jar"
  exit 1
fi

echo "found version $G"

rm -f debian/squarev/var/lib/squarev/*.jar
cp -v build/libs/SQuaReV-$G.jar debian/squarev/var/lib/squarev

sed -i debian/squarev/DEBIAN/control -e "s/VERSION/$G/g"
sed -i debian/squarev/usr/bin/squarev -e "s/VERSION/$G/g"

V=$(grep "Version" debian/squarev/DEBIAN/control | sed -e "s/Version: //g")

fakeroot dpkg-deb -b -Zxz debian/squarev debian/squarev_$V.deb
