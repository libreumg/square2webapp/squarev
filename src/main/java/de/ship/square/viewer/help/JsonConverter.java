package de.ship.square.viewer.help;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.ship.square.viewer.modules.report.ReportBean;

/**
 * 
 * @author henkej
 *
 */
public class JsonConverter {
	
	private final static Logger LOGGER = LogManager.getLogger(JsonConverter.class);
	
	/**
	 * try to convert a json object into a list of report beans
	 * 
	 * @param json the json string
	 * @return the report beans as list; an empty list at least
	 */
	public static final List<ReportBean> toReportBeans(String json) {
		Gson gson = new GsonBuilder().create();
		Type typeToken = new TypeToken<List<ReportBean>>(){}.getType();
		LOGGER.debug("try to convert {}", json);
		List<ReportBean> list = gson.fromJson(json, typeToken);
		return list;
	}
}
