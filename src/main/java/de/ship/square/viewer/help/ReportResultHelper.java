package de.ship.square.viewer.help;

/**
 * 
 * @author henkej
 *
 */
public class ReportResultHelper {
	/**
	 * prepend the id with leading zeros so that level is the number of digits
	 * 
	 * @param id the id
	 * @param level the number of digits
	 * @return the leading zero String of id
	 */
	protected final static String lz(Integer id, Integer level) {
		return String.format("%0" + level + "d", id);
	}
	
	/**
	 * generate the range for the ID due to the level
	 * 
	 * @param id the ID
	 * @param level the level
	 * @return the range
	 */
	protected final static String findDots(Integer id, Integer level) {
		StringBuilder buf = new StringBuilder();
		if (level < 2) {
			return buf.append(id).toString();
		} else {
			Integer factor = Double.valueOf(Math.pow(10, level - 1)).intValue();
			Integer lower = (id / factor) * factor;
			Integer upper = lower + (factor - 1);
			return buf.append(lz(lower, level)).append("..").append(lz(upper, level)).toString();
		}
	}
	
	/**
	 * generate the ID paths
	 * 
	 * @param id the ID
	 * @return the ID paths
	 * @throws NullPointerException if ID is null
	 */
	private final static String generateIdPaths(Integer id) throws NullPointerException {
		if (id == null) {
			throw new NullPointerException("id must not be null");
		}
		StringBuilder buf = new StringBuilder();
		for (int i = 4; i > 0; i--) {
			buf.append("/").append(findDots(id, i));
		}
		return buf.toString();
	}
	

	/**
	 * generate the right absolute report result path
	 * 
	 * @param weburlprefix the prefix to the folder 0000..0999
	 * @param id the ID of the report
	 * @return the generated folder path
	 */
	public final static StringBuilder generateFolder(String weburlprefix, Integer id) {
		StringBuilder buf = new StringBuilder();
		buf.append(weburlprefix);
		buf.append(ReportResultHelper.generateIdPaths(id));
		return buf;
	}
}
