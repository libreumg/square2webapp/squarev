package de.ship.square.viewer.help;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * 
 * @author henkej
 *
 */
public class MimeTypeDetector {
	/**
	 * try to find the right mime type of file
	 * 
	 * @param file the file
	 * @return the estimated mime type
	 * @throws IOException on io errors
	 */
	public static final String findMimeType(File file) throws IOException {
		return file == null ? null : Files.probeContentType(file.toPath());
	}
}
