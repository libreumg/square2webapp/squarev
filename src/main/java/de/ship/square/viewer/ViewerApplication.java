package de.ship.square.viewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author henkej
 *
 */
@SpringBootApplication
public class ViewerApplication{

	public static void main(String[] args) {
		SpringApplication.run(ViewerApplication.class, args);
	}

}
