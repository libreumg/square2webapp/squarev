package de.ship.square.viewer.modules.report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.ship.square.viewer.help.ReportResultHelper;
import de.ship.square.viewer.modules.ISessionService;
import de.ship.square.viewer.modules.db.UserReportGateway;
import de.ship.square.viewer.modules.r.control.SquareControl2Gateway;
import de.ship.square.viewer.modules.r.control.config.ConfigurationInterface;
import de.ship.square.viewer.modules.r.control.config.SquareReportRendererConfiguration;

/**
 * 
 * @author henkej
 *
 */
@Service
public class ReportService implements ISessionService {
	private static final Logger LOGGER = LogManager.getLogger(ReportService.class);

	@Autowired
	private UserReportGateway userReportGateway;

	@Value("${rserver.host:localhost}")
	private String host;

	@Value("${rserver.port:6311}")
	private String portString;

	@Value("${rserver.ssl:false}")
	private String ssl;

	@Value("${rserver.config_filename:/etc/squaredbconnector.properties}")
	private String configFilename;

	@Value("${rserver.config_section:prod}")
	private String configSection;

	@Value("${rserver.output_path:/opt/tmp}")
	private String outputPath;
	
	@Value("${rserver.weburlprefix}")
	private String weburlprefix;

	/**
	 * get the report with all of its result document links
	 * 
	 * @param id the id of the report
	 * @return the report bean
	 * @throws REXPMismatchException on errors
	 * @throws REngineException      on errors
	 * @throws IOException           on errors
	 */
	public ReportBean getReport(Integer id)
			throws IOException, REngineException, REXPMismatchException {
		ReportBean bean = userReportGateway.getUserReport(id);
		SquareControl2Gateway gw = new SquareControl2Gateway(host, portString, ssl);
		ConfigurationInterface config = new SquareReportRendererConfiguration(configFilename, configSection, id,
				outputPath);
		List<String> filePaths = gw.callReports(config);
		LOGGER.debug("found filepaths: {}", filePaths.toArray().toString());
		List<FilePathBean> filePathBeans = new ArrayList<>();
		for (String filePath : filePaths) {
			StringBuilder folder = ReportResultHelper.generateFolder(weburlprefix, id);
			String link = folder.append("/").append(filePath).toString();
			filePathBeans.add(new FilePathBean(link, filePath));
		}
		bean.resetFilePaths(filePathBeans);
		return bean;
	}

}
