package de.ship.square.viewer.modules.r.control;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

/**
 * 
 * @author henkej
 *
 */
public abstract class RGatewayInterface {

	private static final Logger LOGGER = LogManager.getLogger(RGatewayInterface.class);

	/**
	 * get the R connection
	 * 
	 * @param host       the host
	 * @param portString the port as string
	 * @param ssl        the ssl; true or whatever for false
	 * 
	 * @return the R connection
	 * @throws IOException     on io errors
	 * @throws RserveException on r connection errors
	 */
	protected static final RConnection getConnection(String host, String portString, String ssl)
			throws RserveException, IOException {
		LOGGER.debug("connect to {}:{}, ssl = {}", host, portString, ssl);
		return ConnectionManager.getConnection(host, portString, ssl);
	}

	/**
	 * get the R connection
	 * 
	 * @return
	 * @throws IOException     on r connection errors
	 * @throws RserveException on io errors
	 */
	public abstract RConnection getConnection() throws RserveException, IOException;

	/**
	 * load the corresponding library
	 * 
	 * @param libname the name of the R library to load
	 * 
	 * @return the R connection
	 * @throws RserveException on r connection errors
	 * @throws IOException     on io errors
	 */
	public RConnection loadLibrary(String libname) throws RserveException, IOException {
		RConnection connection = getConnection();
		try {
			LOGGER.debug("load library {}", libname);
			connection.parseAndEval("library(" + libname + ")");
		} catch (REngineException | REXPMismatchException e) {
			throw new RserveException(connection, "could not load library " + libname);
		}
		return connection;
	}

	/**
	 * load the library of the gateway; obviously should call loadLibrary(libname)
	 * 
	 * @return the R connection
	 * @throws RserveException on r connection errors
	 * @throws IOException     on io errors
	 */
	public abstract RConnection loadLibrary() throws RserveException, IOException;
}
