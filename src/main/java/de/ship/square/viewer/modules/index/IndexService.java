package de.ship.square.viewer.modules.index;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ship.square.viewer.modules.ISessionService;
import de.ship.square.viewer.modules.db.UserReportGateway;
import de.ship.square.viewer.modules.report.ReportBean;

/**
 * 
 * @author henkej
 *
 */
@Service
public class IndexService implements ISessionService {

	@Autowired
	private UserReportGateway userReportGateway;
	
	/**
	 * get the reports of a user
	 * 
	 * @param username the name of the user
	 * @return the reports; null on an error
	 */
	public List<ReportBean> getReports(String username) {
		return userReportGateway.getUserReports(username);
	}
}
