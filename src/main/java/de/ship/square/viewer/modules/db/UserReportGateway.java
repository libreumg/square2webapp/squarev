package de.ship.square.viewer.modules.db;

import static de.ship.dbppsquare.square.Tables.R_USER_REPORTS;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record6;
import org.jooq.Record7;
import org.jooq.SelectConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.square.viewer.common.SquareGateway;
import de.ship.square.viewer.modules.report.ReportBean;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class UserReportGateway extends SquareGateway {
	private final static Logger LOGGER = LogManager.getLogger(UserReportGateway.class);

	private final DSLContext jooq;

	public UserReportGateway(@Autowired DSLContext jooq) {
		this.jooq = jooq;
		// TODO: implement version check against super.getVersion(jooq);
	}

	/**
	 * get all user reports directly from the database
	 * 
	 * @param username the username
	 * @return the found reports
	 */
	public List<ReportBean> getUserReports(String username) {
		SelectConditionStep<Record7<Integer, String, String, String, String, String, Integer>> sql = jooq
		// @formatter:off
			.selectDistinct(R_USER_REPORTS.ID, 
									    R_USER_REPORTS.NAME, 
									    R_USER_REPORTS.DESCRIPTION, 
									    R_USER_REPORTS.VARGROUP,
										  R_USER_REPORTS.FUNCTIONS, 
										  R_USER_REPORTS.RELEASE, 
										  R_USER_REPORTS.DURATIONS)
				.from(R_USER_REPORTS)
				.where(R_USER_REPORTS.DIRECTUSER.eq(username))
				.or(R_USER_REPORTS.GROUPUSER.eq(username));
		// @formatter:on
		LOGGER.debug(sql.toString());
		List<ReportBean> list = new ArrayList<>();
		for (Record7<Integer, String, String, String, String, String, Integer> r : sql.fetch()) {
			Integer id = r.get(R_USER_REPORTS.ID);
			String name = r.get(R_USER_REPORTS.NAME);
			ReportBean bean = new ReportBean(id, name);
			bean.setDescription(r.get(R_USER_REPORTS.DESCRIPTION));
			bean.setVargroup(r.get(R_USER_REPORTS.VARGROUP));
			bean.setFunctions(r.get(R_USER_REPORTS.FUNCTIONS));
			bean.setRelease(r.get(R_USER_REPORTS.RELEASE));
			bean.setDurations(r.get(R_USER_REPORTS.DURATIONS));
			list.add(bean);
		}
		return list;
	}

	/**
	 * get the user report from the database
	 * 
	 * @param id the ID of the report
	 * @return the user report or null
	 */
	public ReportBean getUserReport(Integer id) {
		SelectConditionStep<Record6<String, String, String, String, String, Integer>> sql = jooq
		// @formatter:off
			.selectDistinct(R_USER_REPORTS.NAME, 
									    R_USER_REPORTS.DESCRIPTION, 
									    R_USER_REPORTS.VARGROUP,
										  R_USER_REPORTS.FUNCTIONS, 
										  R_USER_REPORTS.RELEASE, 
										  R_USER_REPORTS.DURATIONS)
				.from(R_USER_REPORTS)
				.where(R_USER_REPORTS.ID.eq(id));
		// @formatter:on
		LOGGER.debug(sql.toString());
		for (Record6<String, String, String, String, String, Integer> r : sql.fetch()) {
			String name = r.get(R_USER_REPORTS.NAME);
			ReportBean bean = new ReportBean(id, name);
			bean.setDescription(r.get(R_USER_REPORTS.DESCRIPTION));
			bean.setVargroup(r.get(R_USER_REPORTS.VARGROUP));
			bean.setFunctions(r.get(R_USER_REPORTS.FUNCTIONS));
			bean.setRelease(r.get(R_USER_REPORTS.RELEASE));
			bean.setDurations(r.get(R_USER_REPORTS.DURATIONS));
			return bean;
		}
		return null;
	}

}
