package de.ship.square.viewer.modules.r.control.config;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * 
 * @author henkej
 *
 */
public class SquareReportRendererConfiguration implements ConfigurationInterface {
	private JsonObject configuration;
	
	/**
	 * @param configFilename the config filename
	 * @param configSection the config section
	 * @param reportId the ID of the report
	 * @param outputPath the output path
	 */
	public SquareReportRendererConfiguration(String configFilename, String configSection, Integer reportId, String outputPath) {
		configuration = new JsonObject();
		JsonObject repo1 = new JsonObject();
		repo1.addProperty(".class", "sq2psqlRepository"); // TODO: specify by the application (choose a repository)
		repo1.addProperty("config_filename", configFilename);
		repo1.addProperty("config_section", configSection); 
		repo1.addProperty("report_id", reportId);
		repo1.addProperty("output_path", outputPath);
		String REPO1 = "repo1";
		configuration.add(REPO1, repo1);
		configuration.addProperty(".SquareResultIO", REPO1); // TODO: specify by the application (choose a repository)
		configuration.addProperty(".squaresnippletio", REPO1); // TODO: specify by the application (choose a repository)
		configuration.addProperty(".SquareProcessControl", REPO1); // TODO: specify by the application (choose a repository)
		configuration.addProperty(".SquareReportPropIO", REPO1); // TODO: specify by the application (choose a repository)
	}
	
	@Override
	public String getConfiguration() {
		return new Gson().toJson(configuration);
	}

	@Override
	public void updateRepository(boolean overwriteIfExists, String repositoryName, String alias,
			JsonObject repositoryDefinition) {
		JsonObject spec = (JsonObject) configuration.get(alias);
		if (spec == null || overwriteIfExists) {
			spec = repositoryDefinition;
		}
		// if alias exists, GSON will replace the old value (proven by JUnit test, see TestRCommandBuilder.testJsonObjectAdd)
		configuration.add(alias, spec);
		configuration.addProperty(repositoryName, alias);
	}
}
