package de.ship.square.viewer.modules.r.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import com.google.gson.JsonSyntaxException;

import de.ship.square.viewer.help.JsonConverter;
import de.ship.square.viewer.modules.r.control.cmdbuilder.BooleanArgument;
import de.ship.square.viewer.modules.r.control.cmdbuilder.RCommandBuilder;
import de.ship.square.viewer.modules.r.control.cmdbuilder.StringArgument;
import de.ship.square.viewer.modules.r.control.config.ConfigurationInterface;
import de.ship.square.viewer.modules.r.control.patch.NamedValuesetBean;
import de.ship.square.viewer.modules.r.control.patch.RosudaPatcher;
import de.ship.square.viewer.modules.report.ReportBean;
import de.ship.square.viewer.modules.report.ReportStatsBean;

/**
 * 
 * @author henkej
 *
 */
public class SquareControl2Gateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(SquareControl2Gateway.class);
	/**
	 * the R library name
	 */
	public static final String LIBNAME = "SquareControl2";
	/**
	 * minimal version
	 */
	public static final String VERSION = "1.4.1";

	private final String host;
	private final String portString;
	private final String ssl;

	public SquareControl2Gateway(String host, String portString, String ssl) {
		this.host = host;
		this.portString = portString;
		this.ssl = ssl;
	}

	@Override
	public RConnection getConnection() throws RserveException, IOException {
		return getConnection(host, portString, ssl);
	}

	/**
	 * load libary from servlet context
	 * 
	 * @param ctx the servlet context
	 * @return the connection
	 * @throws IOException on io exceptions
	 */
	public final RConnection loadLibrary(ServletContext ctx) throws IOException {
		RConnection connection;
		try {
			connection = getConnection();
		} catch (RserveException e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
		REXP rexp;
		try {
			rexp = connection.parseAndEval("try(library(" + LIBNAME + "))");
			if (!rexp.isString() || !LIBNAME.equals(rexp.asString())) {
				throw new IOException("loaded library " + LIBNAME + " and got " + rexp.asString());
			} else {
				LOGGER.debug("successfully loaded library {}", LIBNAME);
			}
		} catch (REngineException | REXPMismatchException e) {
			LOGGER.error(e.getMessage(), e);
			return connection;
		}
		return connection;
	}

	@Override
	public RConnection loadLibrary() throws IOException {
		RConnection connection;
		try {
			connection = getConnection();
		} catch (RserveException e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
		REXP rexp;
		try {
			rexp = connection.parseAndEval("try(library(" + LIBNAME + "))");
			if (!rexp.isString() || !LIBNAME.equals(rexp.asString())) {
				throw new IOException("loaded library " + LIBNAME + " and got " + rexp.asString());
			} else {
				LOGGER.debug("successfully loaded library {}", LIBNAME);
			}
		} catch (REngineException | REXPMismatchException e) {
			LOGGER.error(e.getMessage(), e);
			return connection;
		}
		return connection;
	}

	/**
	 * call the function print on the R server
	 * 
	 * @param configFile the config file
	 * @return the result
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException      on R engine errors
	 * @throws IOException           on io errors
	 */
	public String getTest(String configFile) throws REngineException, REXPMismatchException, IOException {
		RConnection connection = loadLibrary();
		StringBuilder buf = new StringBuilder("print(\"");
		buf.append("R is up and running");
		buf.append("\")");
		REXP res = connection.parseAndEval(buf.toString());
		return res.asString();
	}

	private REXP execInR(RConnection con, String command) throws REngineException, REXPMismatchException {
		LOGGER.debug("execute in R: {}", command);
		return con.parseAndEval(command);
	}

	/**
	 * get all reports of a user
	 * 
	 * @param reportconfig the report config
	 * @param username     the username
	 * @return the list of found reports
	 * @throws IOException           on IO errors
	 * @throws REXPMismatchException on R mismatch errors
	 * @throws REngineException      on R engine errors
	 * 
	 * @deprecated because R is too slow, direct database connection is used
	 */
	@Deprecated
	public List<ReportBean> getUserReports(ConfigurationInterface reportconfig, String username)
			throws IOException, REngineException, REXPMismatchException, JsonSyntaxException {
		RConnection connection = loadLibrary();
		if (connection == null) {
			LOGGER.error("could not connect to the R server, aborting...");
			return new ArrayList<>();
		}
		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_user_reports", new StringArgument("reportconfig", reportconfig.getConfiguration()),
				new StringArgument("username", username));
		LOGGER.debug("call {}", buf.toString());
		REXP res = execInR(connection, buf.toString());
		if (res.isNull()) {
			LOGGER.warn("getting reports for user {} is null", username);
			return new ArrayList<>();
		} else if (res.isString()) {
			String json = res.asString();
			return JsonConverter.toReportBeans(json);
		} else {
			LOGGER.warn("result is {}", res.asStrings().toString());
			return new ArrayList<>();
		}
	}

	/**
	 * call all reports for the report in reportconfig's report_id
	 * 
	 * @param reportconfig the configuration
	 * 
	 * @return list of found report file names; an empty list at least
	 * 
	 * @throws IOException           on IO errors
	 * @throws REXPMismatchException on R mismatch errors
	 * @throws REngineException      on R engine errors
	 */
	public List<String> callReports(ConfigurationInterface reportconfig)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		if (connection == null) {
			throw new IOException("no connection found; is the R server up and running?");
		}
  	// TODO: get admin argument from keycloak somehow
		Boolean isAdmin = false;
		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_reports", new StringArgument("reportconfig", reportconfig.getConfiguration()),
				new StringArgument("render_args", null, false), new BooleanArgument("admin", isAdmin, false));
																																																			
		REXP res = execInR(connection, buf.toString());
		if (res.isNull()) { // this is the standard result if no result has been found
			return new ArrayList<>();
		} else if (!res.isVector()) {
			LOGGER.error("{} did not return a vector, but a {}", buf.toString(), res.getClass().getSimpleName());
			return new ArrayList<>();
		} else {
			LOGGER.debug("result is {}", res.asStrings().toString());
			String[] fileNames = res.asStrings();
			return Arrays.asList(fileNames);
		}
	}

	/**
	 * get the statistics of the run for a report
	 * 
	 * @param conf the configuration
	 * 
	 * @return the statistics
	 * @throws IOException           on IO errors
	 * @throws REXPMismatchException on R mismatch exceptions
	 * @throws REngineException      on R engine exceptions
	 */
	public ReportStatsBean getStats(ConfigurationInterface conf)
			throws IOException, REngineException, REXPMismatchException {
		de.ship.square.viewer.modules.r.control.cmdbuilder.RCommandBuilder cmd = new de.ship.square.viewer.modules.r.control.cmdbuilder.RCommandBuilder(
				true);
		cmd.appendFunction("call_progress", new StringArgument("reportconfig", conf.getConfiguration()));
		LOGGER.debug(cmd.toString());
		RConnection connection = loadLibrary();
		REXP df = connection.parseAndEval(cmd.toString());

		if (!df.isList()) {
			throw new IOException(df.toDebugString());
		} else {
			RList l = df.asList();
			List<NamedValuesetBean> mapList = new RosudaPatcher().getListOfMapsFromRlist(l);
			List<ReportStatsBean> list = new ArrayList<>();
			for (NamedValuesetBean entry : mapList) {
				Integer rid = entry.getInteger("report_id");
				Double progress = entry.getDouble("progress");
				String message = entry.getString("message");
				Boolean cancelled = entry.getBoolean("cancelled");
				ReportStatsBean bean = new ReportStatsBean(rid, progress, message, cancelled);
				list.add(bean);
			}
			return list.get(0); // use first result, all other are crap
		}
	}
}
