package de.ship.square.viewer.modules.r.control.cmdbuilder;

/**
 * 
 * @author henkej
 *
 */
public class StringArgument implements Argument {

	private final String key;
	private final String value;
	private final Boolean forgetOnNull;

	/**
	 * @param key the key
	 * @param value the value
	 */
	public StringArgument(String key, String value) {
		this.key = key;
		this.value = value;
		this.forgetOnNull = true;
	}
	
	/**
	 * @param key the key
	 * @param value the value
	 * @param printOnNull if value == null, print key = NULL to call
	 */
	public StringArgument(String key, String value, Boolean printOnNull) {
		this.key = key;
		this.value = value;
		this.forgetOnNull = !printOnNull;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(key).append(" = ");
		if (value != null) {
			buf.append("'").append(value).append("'");
		} else {
			buf.append("NULL");
		}
		return buf.toString();
	}
	
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public Boolean getForgetOnNull() {
		return forgetOnNull;
	}
}
