package de.ship.square.viewer.modules.r.control.cmdbuilder;

/**
 * 
 * @author henkej
 *
 */
public interface Argument {
	/**
	 * @return the key
	 */
	public String getKey();

	/**
	 * @return the value
	 */
	public Object getValue();
	
	/**
	 * @return the representation of the argument
	 */
	public String toString();
	
	/**
	 * @return the forgetOnNull flag; if this is true, key == value for value == null should not be added to the statement
	 */
	public Boolean getForgetOnNull();
	
	/**
	 * @return true if value is null and argument should not be added to the statement on null
	 */
	public default Boolean forgetOnNullAndIsNull() {
		return getValue() == null && getForgetOnNull();
	}
}
