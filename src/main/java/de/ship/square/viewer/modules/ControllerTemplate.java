package de.ship.square.viewer.modules;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Logger;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;

import ch.qos.logback.classic.Level;
import de.ship.square.viewer.common.SessionBean;
import de.ship.square.viewer.modules.menu.MenuitemBeanConverter;

/**
 * 
 * @author henkej
 *
 */
public abstract class ControllerTemplate {
	private static final String APPID = "SQuaReV";

	/**
	 * setup the user for the session.
	 * 
	 * @param request      the request
	 * @param indexService the servicee
	 * @param sessionBean  the bean
	 */
	public void setupUserSession(HttpServletRequest request, ISessionService indexService, SessionBean sessionBean) {
		String username = indexService.getUsername(request);
		sessionBean.setUsername(username);
		sessionBean.setIsAdmin(indexService.getRoles(request).contains("admin"));
	}

	/**
	 * setup the session
	 * 
	 * @param model   the model
	 * @param request the request
	 */
	public void setupSession(Model model, HttpServletRequest request, SessionBean sessionBean,
			ISessionService sessionService, String menuurl, MenuitemBeanConverter menuitemBeanConverter) {
		sessionBean.setUsername(sessionService.getUsername(request));
		sessionBean.setIsAdmin(sessionService.getRoles(request).contains("admin"));
		Logger logger = (Logger) LoggerFactory.getILoggerFactory().getLogger("de.ship.square2");
		sessionBean
				.setLogging(logger == null || logger.getLevel() == null ? Level.INFO.levelStr : logger.getLevel().levelStr);
		Set<String> roles = sessionService.getRoles(request);
		model.addAttribute("menuitems",
				sessionService.getMenu(roles, LocaleContextHolder.getLocale().getLanguage(), menuurl, menuitemBeanConverter));
		model.addAttribute("roles", roles);
		model.addAttribute("appid", APPID);
		model.addAttribute("mainversion", sessionService.getMainVersion(menuurl));
		model.addAttribute("mainurl", menuurl != null ? menuurl.replace("/menu", "") : "");
	}

}
