package de.ship.square.viewer.modules.index;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import de.ship.square.viewer.common.SessionBean;
import de.ship.square.viewer.modules.ControllerTemplate;
import de.ship.square.viewer.modules.ISessionService;
import de.ship.square.viewer.modules.menu.MenuitemBeanConverter;

/**
 * 
 * @author henkej
 *
 */
@Controller
// @RolesAllowed({"read", "write"})
public class IndexController extends ControllerTemplate {
	private final static org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(IndexController.class);

	@Autowired
	private IndexService indexService;
	
	@Autowired
	private ISessionService sessionService;

	@Autowired
	private SessionBean sessionBean;
	
	@Value("${reference.application.menu}")
	private String menuurl;

	@Autowired
	private MenuitemBeanConverter menuitemBeanConverter;

	@GetMapping("/")
	public String homePage(Model model, HttpServletRequest request) {
		super.setupUserSession(request, indexService, sessionBean);
		return "index";
	}

	@GetMapping("/login")
	public String login(HttpServletRequest request, Model model) throws ServletException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, indexService, sessionBean);
		model.addAttribute("reports", indexService.getReports(sessionBean.getUsername()));
		return "welcome";
	}

	@GetMapping("/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}	

	@RequestMapping("/logging")
	public String changeLogLevel(HttpServletRequest request, Model model, @RequestParam String level) {
		Logger logger = (Logger) LoggerFactory.getILoggerFactory().getLogger("root");
		if (logger != null) {
			logger.setLevel(Level.toLevel(level));
			LOGGER.info(String.format("Changed root logger to level %s", level));
			setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		return "logging";
		} else {
			LOGGER.error(String.format("root logger not found, cannot set it to level %s", level));
			return "error";
		}
	}
}
