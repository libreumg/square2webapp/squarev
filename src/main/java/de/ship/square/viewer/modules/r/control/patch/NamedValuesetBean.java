package de.ship.square.viewer.modules.r.control.patch;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author henkej
 *
 */
public class NamedValuesetBean {
	
	private final Map<String, Object> map;
	
	/**
	 * create a new NamedValuesetBean with an empty initialized internal map
	 * 
	 * fill the map with addNamed
	 */
	public NamedValuesetBean() {
		map = new HashMap<>();
	}

	@Override
	public String toString() {
		return map.toString();
	}
	
	private String generateCannotDeliver(String key, String wanted, Object got) {
		StringBuilder buf = new StringBuilder("cannot deliver ");
		buf.append(key);
		buf.append(" as ").append(wanted).append("; it is a ");
		buf.append(got == null ? "null" : got.getClass().getSimpleName());
		return buf.toString();
	}
	
	/**
	 * @param key the key
	 * @return the value
	 */
	public Integer getInteger(String key) {
		Object o = map.get(key);
		if (o == null) {
			return null;
		} else if (o instanceof Integer) {
			return (Integer) map.get(key);
		} else {
			throw new ClassCastException(generateCannotDeliver(key, "Integer", o));
		}
	}

	/**
	 * @param key the key
	 * @return the value
	 */
	public String getString(String key) {
		Object o = map.get(key);
		if (o == null) {
			return null;
		} else if (o instanceof String) {
			return (String) map.get(key);
		} else {
			throw new ClassCastException(generateCannotDeliver(key, "String", o));
		}
	}

	/**
	 * @param key the key
	 * @return the value
	 */
	public Boolean getBoolean(String key) {
		Object o = map.get(key);
		if (o == null) {
			return null;
		} else if (o instanceof Boolean) {
			return (Boolean) map.get(key);
		} else {
			throw new ClassCastException(generateCannotDeliver(key, "Boolean", o));
		}
	}

	/**
	 * @param key the key
	 * @return the value
	 */
	public Double getDouble(String key) {
		Object o = map.get(key);
		if (o == null) {
			return null;
		} else if (o instanceof Double) {
			return (Double) map.get(key);
		} else {
			throw new ClassCastException(generateCannotDeliver(key, "Double", o));
		}
	}

	/**
	 * add a named object
	 * 
	 * @param key the key
	 * @param o the object
	 */
	public void addNamed(String key, Object o) {
		map.put(key, o);
	}
}
