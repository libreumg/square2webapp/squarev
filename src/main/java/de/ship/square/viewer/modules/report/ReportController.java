package de.ship.square.viewer.modules.report;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.ship.square.viewer.common.SessionBean;
import de.ship.square.viewer.modules.ControllerTemplate;
import de.ship.square.viewer.modules.ISessionService;
import de.ship.square.viewer.modules.menu.MenuitemBeanConverter;

/**
 * 
 * @author henkej
 *
 */
@Controller
// @RolesAllowed({"read", "write"})
public class ReportController extends ControllerTemplate {

	@Autowired
	private ReportService reportService;

	@Autowired
	private SessionBean sessionBean;
	@Autowired
	private ISessionService sessionService;

	@Value("${reference.application.menu}")
	private String menuurl;

	@Autowired
	private MenuitemBeanConverter menuitemBeanConverter;

	@GetMapping("/report/{id}")
	public String report(Model model, HttpServletRequest request, @PathVariable Integer id)
			throws IOException, REngineException, REXPMismatchException {
		setupSession(model, request, sessionBean, sessionService, menuurl, menuitemBeanConverter);
		super.setupUserSession(request, reportService, sessionBean);
		model.addAttribute("report", reportService.getReport(id));
		return "report";
	}
}
