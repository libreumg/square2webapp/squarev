package de.ship.square.viewer.modules;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import de.ship.square.viewer.help.MimeTypeDetector;

/**
 * 
 * @author henkej
 *
 */
@RestController
public class DownloadController {

	private final Logger LOGGER = LogManager.getLogger(DownloadController.class);

	@Value("${rserver.weburlprefix}")
	private String weburlprefix;

	@Value("${rserver.output_path:/opt/tmp}")
	private String outputPath;

	@GetMapping(value = "/download/**", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StreamingResponseBody> download(final HttpServletRequest request,
			final HttpServletResponse response) {
		final String path = request.getRequestURI().replace("/SQuaReV/download", "");
		StreamingResponseBody stream = out -> {
			final String fullFilePath = new StringBuilder(outputPath).append(File.separator).append(path).toString().replace("%20", " ");
			final File file = new File(fullFilePath);
			if (file.exists()) {
				if (file.isDirectory()) {
					LOGGER.warn("cannot deliver a folder as output; ignoring {}", fullFilePath);
				} else {
					response.setContentType(MimeTypeDetector.findMimeType(file));
					final OutputStream outStream = new BufferedOutputStream(response.getOutputStream());
					try {
						final InputStream inStream = new FileInputStream(file);
						byte[] bytes = new byte[1024];
						int length;
						while ((length = inStream.read(bytes)) >= 0) {
							outStream.write(bytes, 0, length);
						}
						inStream.close();
					} catch (final IOException e) {
						LOGGER.error("Exception while reading and streaming data {} ", e);
						throw e;
					}
					outStream.close();
				}
			} else {
				LOGGER.error("could not find file '{}'", fullFilePath);
				throw new FileNotFoundException(String.format("could not find file '%s'", fullFilePath));
			}
		};
		return new ResponseEntity<StreamingResponseBody>(stream, HttpStatus.OK);

	}
}