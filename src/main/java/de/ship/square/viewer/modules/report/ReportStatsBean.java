package de.ship.square.viewer.modules.report;

/**
 * 
 * @author henkej
 *
 */
public class ReportStatsBean {
	private final Integer reportId;
	private final Double progress;
	private final String message;
	private final Boolean cancelled;

	/**
	 * @param reportId the report id
	 * @param progress the progress
	 * @param message the message
	 * @param cancelled the cancelled
	 */
	public ReportStatsBean(Integer reportId, Double progress, String message, Boolean cancelled) {
		super();
		this.reportId = reportId;
		this.progress = progress;
		this.message = message;
		this.cancelled = cancelled;
	}

	/**
	 * @return the reportId as string
	 */
	public String getReportIdString() {
		return reportId == null ? "" : reportId.toString();
	}

	/**
	 * @return the progress as string
	 */
	public String getProgressString() {
		return progress == null ? "" : progress.toString();
	}
	
	/**
	 * @return the reportId
	 */
	public Integer getReportId() {
		return reportId;
	}

	/**
	 * @return the progress
	 */
	public Double getProgress() {
		return progress;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the cancelled
	 */
	public Boolean getCancelled() {
		return cancelled;
	}
}
