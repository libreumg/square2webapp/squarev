package de.ship.square.viewer.modules.report;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class FilePathBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String link;
	private final String name;

	public FilePathBean(String link, String name) {
		super();
		this.link = link;
		this.name = name;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
