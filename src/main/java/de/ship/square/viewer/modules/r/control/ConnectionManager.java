package de.ship.square.viewer.modules.r.control;

import java.io.IOException;

import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

/**
 * 
 * @author henkej
 *
 */
public class ConnectionManager {

	/**
	 * we don't need this constructor to be public
	 */
	private ConnectionManager() {
	}

	/**
	 * get a new R connection
	 * 
	 * @param host the host
	 * @param portString the port as string
	 * @param ssl the ssl; true or whatever for false
	 * @return the connection
	 * @throws IOException on io errors
	 * @throws RserveException on rserve exceptions
	 */
	public static final RConnection getConnection(String host, String portString, String ssl)
			throws RserveException, IOException {
		Boolean sslMode = "true".equalsIgnoreCase(ssl);
		host = host == null ? "localhost" : host;
		Integer port = portString == null ? 6311 : Integer.valueOf(portString);
		return sslMode ? new SslRConnection(host, port) : new RConnection(host, port);
	}
}
