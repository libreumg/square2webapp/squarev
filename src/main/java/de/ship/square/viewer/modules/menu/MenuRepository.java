package de.ship.square.viewer.modules.menu;

import static de.ship.dbppsquare.appconfig.Tables.T_MENUROLE;
import static de.ship.dbppsquare.appconfig.Tables.V_MENUITEM;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record5;
import org.jooq.SelectSeekStep1;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.ship.dbppsquare.appconfig.enums.EnumLocale;
import de.ship.dbppsquare.appconfig.tables.records.TMenuroleRecord;
import de.ship.square.viewer.common.SquareGateway;

/**
 * 
 * @author henkej
 *
 */
@Repository
public class MenuRepository extends SquareGateway {
	private final static Logger LOGGER = LogManager.getLogger(MenuRepository.class);

	private final static Integer EXPECTED_VERSION = 77;

	private final DSLContext jooq;

	public MenuRepository(@Autowired DSLContext jooq) {
		this.jooq = jooq;
		Integer version = super.getVersion(jooq);
		if (version < EXPECTED_VERSION) {
			throw new DataAccessException(
					String.format("Database version %d is too slow, need at least %d", version, EXPECTED_VERSION));
		}
	}

	/**
	 * get the menu from the database
	 * 
	 * @param locale the locale to translate the menu to; e.g. de, en, es, pl
	 * 
	 * @return the list of menu entries; an empty list at least
	 */
	public List<MenuitemBean> getMenu(EnumLocale locale) {
		SelectSeekStep1<Record5<String, Integer, String, String, String>, Integer> sql = jooq
		// @formatter:off
			.select(V_MENUITEM.ID, V_MENUITEM.PK, V_MENUITEM.NAME, V_MENUITEM.DESCRIPTION, V_MENUITEM.URL)
			.from(V_MENUITEM)
			.where(V_MENUITEM.LOCALE.eq(locale))
			.orderBy(V_MENUITEM.ORDER_NR);
		// @formatter:on
		LOGGER.debug(sql.toString());
		Map<Integer, Set<String>> roleMapping = new HashMap<>();
		for (TMenuroleRecord menurole : jooq.selectFrom(T_MENUROLE).fetch()) {
			Integer fkMenuitem = menurole.getFkMenuitem();
			String rolename = menurole.getRolename();
			Set<String> set = roleMapping.get(fkMenuitem);
			if (set == null) {
				set = new HashSet<>();
				roleMapping.put(fkMenuitem, set);
			}
			set.add(rolename);
		}
		List<MenuitemBean> list = new ArrayList<>();
		for (Record5<String, Integer, String, String, String> r : sql.fetch()) {
			String id = r.get(V_MENUITEM.ID);
			Integer pk = r.get(V_MENUITEM.PK);
			String name = r.get(V_MENUITEM.NAME);
			String description = r.get(V_MENUITEM.DESCRIPTION);
			String url = r.get(V_MENUITEM.URL);
			Set<String> roles = roleMapping.get(pk);
			MenuitemBean bean = new MenuitemBean();
			bean.setId(id);
			bean.setLabel(name);
			bean.setUrl(url);
			bean.setDescription(description);
			bean.getRoles().addAll(roles);
			list.add(bean);
		}
		return list;
	}

}
