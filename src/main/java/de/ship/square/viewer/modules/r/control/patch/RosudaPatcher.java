package de.ship.square.viewer.modules.r.control.patch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.jooq.exception.DataAccessException;
import org.rosuda.REngine.REXPDouble;
import org.rosuda.REngine.REXPInteger;
import org.rosuda.REngine.REXPLogical;
import org.rosuda.REngine.REXPString;
import org.rosuda.REngine.RList;

/**
 * 
 * @author henkej
 *
 */
public class RosudaPatcher {

	/**
	 * convert an RList to a list of named valueset beans
	 * 
	 * @param l the RList
	 * @return the named valueset beans
	 */
	public List<NamedValuesetBean> getListOfMapsFromRlist(RList l) {
		List<NamedValuesetBean> list = new ArrayList<>();
		Iterator<?> i = l.iterator();
		Vector<?> names = l.names;
		Iterator<?> n = names.iterator();
		String name = "";
		List<NamedValuesetBean> cache = new ArrayList<>();
		while (i.hasNext()) {
			Object column = i.next();
			if (n.hasNext()) {
				name = asString(n.next());
			}
			if (column instanceof REXPDouble) {
				REXPDouble doubleColumn = (REXPDouble) column;
				double[] doubles = doubleColumn.asDoubles();
				for (int rowIndex = 0; rowIndex < doubles.length; rowIndex++) {
					NamedValuesetBean bean = new NamedValuesetBean();
					if (cache.size() > rowIndex) {
						bean = cache.get(rowIndex);
					} else {
						cache.add(bean);
					}
					bean.addNamed(name, doubles[rowIndex]);
				}
				list.addAll(cache);
			} else if (column instanceof REXPLogical) {
				REXPLogical logicalColumn = (REXPLogical) column;
				int[] ints = logicalColumn.asIntegers();
				for (int rowIndex = 0; rowIndex < ints.length; rowIndex++) {
					NamedValuesetBean bean = new NamedValuesetBean();
					if (cache.size() > rowIndex) {
						bean = cache.get(rowIndex);
					} else {
						cache.add(bean);
					}
					bean.addNamed(name, ints[rowIndex] > 0);
				}
				list.addAll(cache);			
			} else if (column instanceof REXPInteger) {
				REXPInteger intColumn = (REXPInteger) column;
				int[] ints = intColumn.asIntegers();
				for (int rowIndex = 0; rowIndex < ints.length; rowIndex++) {
					NamedValuesetBean bean = new NamedValuesetBean();
					if (cache.size() > rowIndex) {
						bean = cache.get(rowIndex);
					} else {
						cache.add(bean);
					}
					bean.addNamed(name, ints[rowIndex]);
				}
				list.addAll(cache);
			} else if (column instanceof REXPString) {
				REXPString stringColumn = (REXPString) column;
				String[] strings = stringColumn.asStrings();
				for (int rowIndex = 0; rowIndex < strings.length; rowIndex++) {
					NamedValuesetBean bean = new NamedValuesetBean();
					if (cache.size() > rowIndex) {
						bean = cache.get(rowIndex);
					} else {
						cache.add(bean);
					}
					bean.addNamed(name, strings[rowIndex]);
				}
				list.addAll(cache);
			} else {
				throw new DataAccessException("unsupported R format " + column.getClass().getSimpleName() + " found, aborting...");
			}
		}
		return list;
	}

	private String asString(Object string) {
		if (string == null) {
			return null;
		} else if (string instanceof String) {
			return (String) string;
		} else {
			return string.toString();
		}
	}
}
