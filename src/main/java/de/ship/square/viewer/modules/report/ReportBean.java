package de.ship.square.viewer.modules.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class ReportBean implements Serializable, Comparable<ReportBean> {
	private static final long serialVersionUID = 1L;
	
	private final Integer id;
	private final String name;
	private String description;
	private String functions;
	private String vargroup;
	private Integer durations;
	private String release;
	private final List<FilePathBean> filePaths;

	public ReportBean(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.filePaths = new ArrayList<>();
	}
	
	@Override
	public int compareTo(ReportBean bean) {
		return bean == null || bean.getId() == null ? 0 : bean.getId().compareTo(id);
	}
	
	public void resetFilePaths(List<FilePathBean> filePaths) {
		this.filePaths.clear();
		this.filePaths.addAll(filePaths);
	}
	
	/**
	 * @return the file paths
	 */
	public List<FilePathBean> getFilePaths(){
		return filePaths;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the functions
	 */
	public String getFunctions() {
		return functions;
	}

	/**
	 * @param functions the functions to set
	 */
	public void setFunctions(String functions) {
		this.functions = functions;
	}

	/**
	 * @return the vargroup
	 */
	public String getVargroup() {
		return vargroup;
	}

	/**
	 * @param vargroup the vargroup to set
	 */
	public void setVargroup(String vargroup) {
		this.vargroup = vargroup;
	}

	/**
	 * @return the durations
	 */
	public Integer getDurations() {
		return durations;
	}

	/**
	 * @param durations the durations to set
	 */
	public void setDurations(Integer durations) {
		this.durations = durations;
	}

	/**
	 * @return the release
	 */
	public String getRelease() {
		return release;
	}

	/**
	 * @param release the release to set
	 */
	public void setRelease(String release) {
		this.release = release;
	}
}
