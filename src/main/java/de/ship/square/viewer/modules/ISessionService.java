package de.ship.square.viewer.modules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.IDToken;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.client.RestTemplate;

import de.ship.square.viewer.modules.menu.MenuitemBean;

/**
 * 
 * @author henkej
 *
 */
public interface ISessionService {
	public final static Logger LOGGER = LogManager.getLogger(ISessionService.class);

	/**
	 * Get the id token from the request on keycloak connections
	 *
	 * @param request the request
	 * @return the id token of the keycloak connection
	 */
	public default IDToken getIdTokenFromPrincipal(HttpServletRequest request) {
		KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
		return principal == null ? null : principal.getAccount().getKeycloakSecurityContext().getIdToken();
	}

	/**
	 * Get the username of the logged in user
	 *
	 * @param request the request
	 * @return the username in the id token of the keycloak connection
	 */
	public default String getUsername(HttpServletRequest request) {
		IDToken idToken = getIdTokenFromPrincipal(request);
		return idToken == null ? null : idToken.getPreferredUsername();
	}

	/**
	 * get all roles from keycloak
	 * 
	 * @param request the request
	 * @return the roles from keycloak
	 */
	public default Set<String> getRoles(HttpServletRequest request) {
		KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) request.getUserPrincipal();
		Set<String> roles = new HashSet<>();
		if (principal != null) {
			for (GrantedAuthority granted : principal.getAuthorities()) {
				String r = granted.getAuthority();
				// remove possible keycloak prefix ROLE_
				roles.add(r.startsWith("ROLE_") ? r.replace("ROLE_", "") : r);
			}
		}
		return roles;
	}

	/**
	 * get the menu from the main application
	 * 
	 * @param roles  the user rules
	 * @param locale the locale
	 * @return the menu items
	 */
	public default List<MenuitemBean> getMenu(Set<String> roles, String locale, String menuurl, HttpMessageConverter<?> menuitemBeanConverter) {
		StringBuilder url = new StringBuilder(menuurl);
		url.append("/").append(locale);
		RestTemplate rest = new RestTemplate();
		rest.getMessageConverters().add(menuitemBeanConverter);
		ResponseEntity<MenuitemBean[]> response = rest.getForEntity(url.toString(), MenuitemBean[].class);
		List<MenuitemBean> list = new ArrayList<>();
		for (MenuitemBean bean : response.getBody()) {
			if (bean.hasAnyRole(roles)) {
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * get the version of the main application
	 * 
	 * @param menuurl the URL for the menu
	 * @return the version if found, ? otherwise
	 */
	public default String getMainVersion(String menuurl) {
		String versionurl = menuurl.replace("/menu", "/version");
		try {
			RestTemplate rest = new RestTemplate();
			ResponseEntity<String> response = rest.getForEntity(versionurl, String.class);
			String json = response.getBody();
			return json.replace("{\"version\":\"", "").replace("\"}", "");
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return "?";
	}
}
