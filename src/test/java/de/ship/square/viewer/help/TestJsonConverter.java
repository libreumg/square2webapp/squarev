package de.ship.square.viewer.help;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.ship.square.viewer.modules.report.ReportBean;

/**
 * 
 * @author henkej
 *
 */
public class TestJsonConverter {

	@Test
	public void testToReportBeans() {
		String json = "[{\"id\": 42, \"name\": \"honey\"}]";
		List<ReportBean> list = JsonConverter.toReportBeans(json);
		assertNotNull(list);
		assertEquals(1, list.size());
		for (ReportBean bean : list) {
			assertEquals(42, bean.getId());
			assertEquals("honey", bean.getName());
		}
	}
}
