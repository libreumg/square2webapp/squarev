package de.ship.square.viewer.help;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestReportResultHelper {
	@Test
	public void testGenerateFolder() {
		String expected = "http://x/0000..0999/000..099/00..09/4";
		assertEquals(expected, ReportResultHelper.generateFolder("http://x", 4).toString());
	}
}
